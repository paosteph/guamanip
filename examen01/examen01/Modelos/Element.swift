//
//  Element.swift
//  examen01
//
//  Created by PAOLA GUAMANI on 6/6/18.
//  Copyright © 2018 Pao Stephanye. All rights reserved.
//

import Foundation

struct Element{
    let label: String
    let value: Int
}

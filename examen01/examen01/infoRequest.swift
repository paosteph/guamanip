//
//  infoRequest.swift
//  examen01
//
//  Created by PAOLA GUAMANI on 5/6/18.
//  Copyright © 2018 Pao Stephanye. All rights reserved.
//

import Foundation

struct infoJsonRequest: Decodable {
    let viewTitle: String
    let date: String
    let nextLink: String
    
}


//
//  ListaViewController.swift
//  examen01
//
//  Created by PAOLA GUAMANI on 6/6/18.
//  Copyright © 2018 Pao Stephanye. All rights reserved.
//

import UIKit

class ListaViewController: UIViewController, UITableViewDataSource, UITableViewDelegate  {
    @IBOutlet weak var itemJsonTable: UITableView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var linkLabel: UILabel!
    @IBOutlet weak var averageLabel: UILabel!
    var suma = 0
    var name: String?
    var link: String?
    var listaJson = ListaJson()
    var arrayJson:[Element] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        nameLabel.text = "Hi, \(name ?? "You")"
        linkLabel.text = "Link1 +\(link ?? "http")"
        
        let urlString = "https://api.myjson.com/bins/182sje"
        let url = URL(string: urlString)
        let session = URLSession.shared
        
        let task = session.dataTask(with: url!){ (data, response, error)
            in
            guard let data = data else{
                print("Error NO data")
                return
            }
            guard let myInfoJson = try? JSONDecoder().decode(listaJsonRequest.self, from: data) else{
                print("Error decoding json")
                return
            }
            
            DispatchQueue.main.async {
                for data in myInfoJson.data {
                    self.arrayJson.append(Element(label:data.label, value: data.value))
                    self.itemJsonTable.reloadData()
                    self.suma += data.value
                    print("Suma ",self.suma)
                }
                self.suma = self.suma/4
                print("Promedio ",self.suma)
                self.averageLabel.text = String(self.suma)
            }
        }
        task.resume()
        self.itemJsonTable.reloadData()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        itemJsonTable.reloadData()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayJson.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "itemCell") as! ItemTableViewCell
        
        if indexPath.section == 0 {
            cell.labelCell.text = arrayJson[indexPath.row].label
            cell.valueCell.text = String(arrayJson[indexPath.row].value)
        }
        
        return cell
    }

    

    override func didReceiveMemoryWarning() {
        
    }
    

}

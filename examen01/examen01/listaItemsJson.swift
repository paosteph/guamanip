//
//  listaItemsJson.swift
//  examen01
//
//  Created by PAOLA GUAMANI on 6/6/18.
//  Copyright © 2018 Pao Stephanye. All rights reserved.
//

import Foundation

struct listaJsonRequest: Decodable{
    let data: [itemJson]
    
}

struct itemJson: Decodable {
    let label: String
    let value: Int
    
}

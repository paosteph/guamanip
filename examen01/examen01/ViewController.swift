//
//  ViewController.swift
//  examen01
//
//  Created by PAOLA GUAMANI on 5/6/18.
//  Copyright © 2018 Pao Stephanye. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var viewTitleLabel: UILabel!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var infoLabel: UILabel!
    var nextLink: String?
    
    @IBAction func nextButtonPressed(_ sender: Any) {
        
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toTableItems"{
            let destination = segue.destination as! ListaViewController //segue puede tener donde esta y donde va
            destination.name = nameTextField.text
            destination.link = nextLink
            
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        let urlString = "https://api.myjson.com/bins/72936"
        let url = URL(string: urlString)
        let session = URLSession.shared
        
        let task = session.dataTask(with: url!){ (data, response, error)
            in
            //con guard me aseguro q exista data, y sino termina ahi ejecucion
            guard let data = data else{
                print("Error NO data")
                return
            }
            guard let myInfoJson = try? JSONDecoder().decode(infoJsonRequest.self, from: data) else{
                    print("Error decoding json")
                    return
                }

            DispatchQueue.main.async {
                self.viewTitleLabel.text = "\(myInfoJson.viewTitle)"
                self.infoLabel.text = "\(myInfoJson.date)"
                self.nextLink = myInfoJson.nextLink
            }
            
            
        }
        
        task.resume()
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

